use clap::Parser;
use image::io::Reader as ImageReader;
use image::imageops::FilterType;
use rayon::prelude::*;
use std::{
    io::Cursor,
    path::PathBuf,
};


#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    inputs: Option<PathBuf>,

    #[arg(short, long)]
    dir: Option<PathBuf>,

    #[arg(short, long)]
    output: PathBuf,

    #[arg(short, long, default_value_t = 1.33)]
    ratio: f32,

    #[arg(short, long, default_value_t = true)]
    verbose: bool,
}

fn main() -> Result<(), Box<dyn std::error::Error>>{
    let args = Args::parse();
    
    if args.inputs.is_some() && args.dir.is_some() {
        panic!("-i and -d cannot be used together");
    }

    let images = if let Some(inputs) = args.inputs{
        let images = vec![inputs];
        images
    } else if let Some(dir) = args.dir {
        let entries = std::fs::read_dir(dir).unwrap();
        let mut images = vec![];

        for entry in entries {
            let entry = match entry {
                Ok(o) => o,
                Err(_) => continue,
            };

            let ftype = match entry.file_type() {
                Ok(o) => o,
                Err(_) => continue,
            };

            if !ftype.is_file() {
                continue;
            } else {
                images.push(entry.path());
            }
        }

        images
    } else {
        panic!()
    };

    images
        .iter()
        .for_each(|img_path| {
            let img = image::open(img_path).unwrap();
            
            let resized = img.resize_exact( 
                img.width(),
                (img.height() as f32 * (1.0 / args.ratio)) as u32,
                FilterType::Nearest,
            );

            let file_name = img_path.as_path().file_name().unwrap().to_str().unwrap();
            resized.save(format!("{}/{}", args.output.display(), file_name)).unwrap();
        });

    Ok(())
}
